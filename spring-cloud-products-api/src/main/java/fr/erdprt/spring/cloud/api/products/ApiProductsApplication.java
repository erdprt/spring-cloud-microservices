package fr.erdprt.spring.cloud.api.products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ApiProductsApplication {

    public static void main(String[] args) {
    
        SpringApplication.run(ApiProductsApplication.class, args);
    }
}