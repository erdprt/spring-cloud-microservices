package fr.erdprt.spring.cloud.api.products.controllers;

import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.spring.cloud.api.products.model.Product;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/products")
public class ProductsController {
	
	private static Logger LOGGER	=	LoggerFactory.getLogger(ProductsController.class);

    @GetMapping(value = "/{id}")
    public ResponseEntity<Product> productById(@PathVariable Integer id) {
    	LOGGER.info("call productById for id={}", id);
      return new ResponseEntity<Product>(new Product(id, "product-" + id), HttpStatus.OK);
    }

    
    @GetMapping(value = "/")
    public ResponseEntity<List<Product>> products() {
    	LOGGER.info("call products");
    	List<Product> products	=	new ArrayList<Product>();
    	for (int index=0;index<= 10;index++) {
    		products.add(new Product(index, "product-" + index));
    	}
    	return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

}