package fr.erdprt.spring.cloud.api.products.controllers;

import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.spring.cloud.api.products.model.Product;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.Collections;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;


@RestController
@RequestMapping("/")
public class ApiController {
	
	@Value("${spring.cloud.client.hostname}")
	private String hostName;
	
	private static Logger LOGGER	=	LoggerFactory.getLogger(ApiController.class);

    @GetMapping(value = "/hostname")
    public ResponseEntity<List<String>> ping() {
    	LOGGER.info("call api");
      List<String> list = new ArrayList<String>();
      list.add(hostName);
      return new ResponseEntity<List<String>>(list, HttpStatus.OK);
    }
    
  	@GetMapping("/properties/")
  	@ResponseBody
  	public Map<String, String> properties() {
      return (Map)System.getProperties();
  	}
  
  	@GetMapping("/env/")
  	@ResponseBody
  	public Map<String, String> env() {
      return System.getenv();
  	}
    

}